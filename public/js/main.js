'use strict';

(($) => {
  $(() => {
    const $form = $('.js-product-form');
    const $cart = $('.js-cart');
    const $cartBody = $('.js-cart tbody');
    const $cartClean = $('.js-cart-clean');
    const $cartTotal = $('.js-cart-total');

    /**
     * Add product
     */
    $form.on('submit', async (evt) => {
      evt.preventDefault();

      const formData = getFormData($form);

      // validation
      let isValid = true;
      Object.entries(formData).forEach(([name, value]) => {
        if (!value) {
          const $input = $form.find(`[name="${name}"]`);
          $input.addClass('error');

          // focus first error input
          if (isValid) {
            $input.focus();
          }

          isValid = false;
        }
      });

      if (isValid) {
        try {
          // send request
          const response = await fetch('/api/cart/item', {
            method:  'POST',
            body:    JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          });
          const json = await response.json();

          // process response
          if (response.status !== 200) {
            if (json.field) {
              $form.find(`[name="${json.field}"]`)
                .addClass('error')
                .focus();
            }

            showError(json);
          } else {
            $form.get(0).reset();
            buildCart(json);
          }
        } catch (err) {
          showError(err);
        }
      }
    });

    /**
     * Reset error state
     */
    const resetError = (evt) => {
      $(evt.currentTarget).removeClass('error');
    };
    $form
      .on('keydown', '.error', resetError)
      .on('change', '.error', resetError);

    /**
     * Remove product
     */
    $cartBody.on('click', '.js-cart-item-remove', async (evt) => {
      evt.preventDefault();

      try {
        const $row = $(evt.currentTarget).closest('tr');
        const id = $row.data('id');

        // send request
        const method = 'DELETE';
        const response = await fetch(`/api/cart/item/${id}`, { method });
        const data = await response.json();

        // process response
        if (response.status !== 200) {
          showError(data);
        } else {
          buildCart(data);
        }
      } catch (err) {
        showError(err);
      }
    });

    /**
     * Update cart
     */
    const updateCart = async () => {
      try {
        // send request
        const response = await fetch('/api/cart');
        const data = await response.json();

        // process response
        if (response.status !== 200) {
          showError(data);
        } else {
          buildCart(data);
        }
      } catch (err) {
        showError(err);
      }
    };

    /**
     * Clean cart
     */
    $cartClean.on('click', async (evt) => {
      evt.preventDefault();

      // send request
      try {
        const method = 'DELETE';
        const response = await fetch('/api/cart', { method });
        const data = await response.json();

        // process response
        if (response.status !== 200) {
          showError(data);
        } else {
          cleanCart();
        }
      } catch (err) {
        showError(err);
      }
    });

    /**
     * Get form values as JSON-object
     * @param {object} $form
     * @returns {{}}
     */
    const getFormData = ($form) => {
      const result = {};

      $form.serializeArray().forEach((item) => {
        result[item.name] = item.value.trim();
      });

      return result;
    };

    /**
     * Build cart
     * @param {object} data cart data
     */
    const buildCart = (data) => {
      /**
       * Build cart lines HTML
       * @param {[*]} items products
       * @returns {string}
       */
      const buildItemsHTML = (items) => {
        return items
          .map((item) => {
            item.priceFormatted = formatPrice(item.price);
            item.totalFormatted = formatPrice(item.total);
            return item;
          }).map(({ id, name, priceFormatted, currency, quantity, totalFormatted }) => (
            `<tr data-id="${id}">` +
              `<td>${name}</td>` +
              `<td>${priceFormatted} ${currency}</td>` +
              `<td>${quantity}</td>` +
              `<td>${totalFormatted} ${currency}</td>` +
              `<td class="remove-col"><span class="cart-item-remove js-cart-item-remove"></span></tdclass>` +
            `</tr>`
          )).join('');
      };

      /**
       * Build total line HTML
       * @param {object} total sums by currencies
       * @returns {string}
       */
      const buildTotalHTML = (total) => {
        return total.map(({ currency, value }) => (
          `${formatPrice(value)} ${currency}`
        )).join('<br />');
      };

      $cart.toggle(data.items.length > 0);
      $cartBody.html(buildItemsHTML(data.items));
      $cartTotal.html(buildTotalHTML(data.total));
    };

    /**
     * Clean cart
     */
    const cleanCart = () => {
      $cart.hide();
      $cartBody.html('');
    };

    updateCart();
  });
})(jQuery);
