// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

/**
 * Price formatter
 * @param {number} price
 * @returns {string}
 */
const formatPrice = (price) => {
  return price.toFixed(2);
};

(($) => {
  window.showError = (error) => {
    const message = 'message' in error ? error.message : error;

    $.miniNoty({
      view:          'error',
      message:       `Error: ${message}`,
      timeoutToHide: 5000,
    });
  };
})(jQuery);
