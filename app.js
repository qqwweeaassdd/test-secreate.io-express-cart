const config = require('config');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');

mongoose.connect(config.get('mongodb.connectionString'), {
  useNewUrlParser:    true,
  useUnifiedTopology: true,
});
mongoose.Promise = global.Promise;

const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
  secret:            config.get('mongodb.secret'),
  resave:            false,
  saveUninitialized: true,
  store:             new MongoStore({ mongooseConnection: mongoose.connection }),
}));

app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  sourceMap: true,
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  const isDev = (req.app.get('env') === 'development');
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = isDev ? err : {};

  // render the error page
  const statusCode = err.status || 500;
  res.status(statusCode);

  if (req.locals && req.locals.isJSON) {
    res.json({
      name:    err.name,
      message: err.message,
      status:  statusCode,
      field:   err.field,
      stack:   isDev ? err.stack : '',
    });
  } else {
    res.render('error');
  }
});

module.exports = app;
