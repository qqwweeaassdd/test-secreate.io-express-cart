'use strict';

const express = require('express');

const Cart = require('../lib/cart');

const router = express.Router();

/**
 * Set request type to JSON for all API methods
 */
router.all('*', (req, res, next) => {
  req.locals = req.locals || {};
  req.locals.isJSON = true;
  req.locals.cart = Cart(req.session);
  next();
});

/**
 * Get cart
 */
router.get('/cart', (req, res, next) => {
  res.json(req.locals.cart.get());
});

/**
 * Clean cart
 */
router.delete('/cart', (req, res, next) => {
  req.locals.cart.clean();
  res.json({});
});

/**
 * Add product
 */
router.post('/cart/item', (req, res, next) => {
  const cart = req.locals.cart;
  cart.addItem(req.body);
  res.json(cart.get());
});

/**
 * Delete product
 */
router.delete('/cart/item/:id([a-z0-9-]+)', (req, res, next) => {
  const cart = req.locals.cart;
  cart.deleteItem(req.params.id);
  res.json(cart.get());
});

module.exports = router;
