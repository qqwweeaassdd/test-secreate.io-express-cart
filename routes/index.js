'use strict';

const express = require('express');

const Currency = require('../lib/currency');

const router = express.Router();

/**
 * Main page
 */
router.get('/', (req, res, next) => {
  res.render('index', {
    title: 'Add product to cart',
    currencies: Currency.getCurrList(),
  });
});

module.exports = router;
