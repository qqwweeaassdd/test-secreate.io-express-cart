'use strict';

const uuidv1 = require('uuid/v1');
const config = require('config');

const Helpers = require('../lib/helpers');
const UserError = require('../lib/error');
const Currency = require('./currency');

const cartConfig = config.get('cart');

/**
 * Cart
 */
module.exports = (session) => {
  /**
   * Check product's fields. Return correct product object
   *    Throws error when product fields are invalid
   * @param {object} item
   * @returns {{}}
   */
  const processItem = (item) => {
    const result = {
      id: uuidv1(),
    };

    const requiredFields = ['name', 'price', 'currency', 'quantity'];
    requiredFields.forEach((field) => {
      if (!item[field]) {
        throw new UserError(500, field, `Field "${field}" is required`);
      }
      result[field] = ('' + item[field]).trim();
    });

    // name
    if (!result.name) {
      throw new UserError(500, 'name', `Product name is empty`);
    } else if (result.name.length > cartConfig.nameMaxLength) {
      throw new UserError(500, 'name', `Product name is too long. Max length is ${cartConfig.nameMaxLength}`);
    } else {
      result.name = Helpers.escapeHTML(result.name);
    }

    // price
    result.price = result.price.replace(',', '.');
    const price = Number(result.price);
    if (!price) {
      throw new UserError(500, 'price', `Price format invalid: "${item.price}"`);
    } else if (price < cartConfig.priceMin || price > cartConfig.priceMax) {
      throw new UserError(500, 'price', `Price range error: "${item.price}". Available price range: [${cartConfig.priceMin}, ${cartConfig.priceMax}]`);
    }
    result.price = Helpers.formatPrice(price);

    // currency
    if (!Currency.checkCurrency(result.currency)) {
      throw new UserError(500, 'currency', `Currency is not available: "${item.currency}". Available currencies list is [${Currency.getCurrList().join(',')}]`);
    }

    // quantity
    const quantity = Number(result.quantity);
    if (!quantity || !result.quantity.match(/^\d+$/)) {
      throw new UserError(500, 'quantity', `Quantity format invalid: "${result.quantity}"`);
    } else if (quantity < cartConfig.quantityMin || quantity > cartConfig.quantityMax) {
      throw new UserError(500, 'quantity', `Quantity range error: "${item.quantity}". Available quantity range: [${cartConfig.quantityMin}, ${cartConfig.quantityMax}]`);
    }
    result.quantity = quantity;

    return result;
  };

  /**
   * Add product to cart
   * @param {object} product
   */
  const addItem = (product) => {
    product = processItem(product);
    getBaseCart().push(product);
  };

  /**
   * Delete product
   * @param {string} id
   */
  const deleteItem = (id) => {
    const cart = getBaseCart();
    const productIdx = cart.findIndex((item) => item.id === id);

    if (productIdx < 0) {
      throw new UserError(500, `Can't find required item`);
    }

    cart.splice(productIdx, 1);
  };

  /**
   * Get cart
   * @returns {[*]}
   */
  const getBaseCart = () => {
    session.cart = session.cart || [];
    return session.cart;
  };

  /**
   * Calculate total cart price for base currency
   * @param {object[]} items cart items
   * @param {string} toCurrency
   * @returns {number}
   */
  const calcTotalForCurrency = (items, toCurrency) => {
    return items.reduce((acc, item) => {
      return acc + Currency.convert(item.total, item.currency, toCurrency);
    }, 0);
  };

  /**
   * Get full cart data
   * @returns {{total: {currency: string, value: number}[], items: (*|{total: number})[]}}
   */
  const get = () => {
    const items = getBaseCart().map((item) => ({
      ...item,
      total: item.quantity * item.price,
    }));

    // calc totals
    const total = Currency.getCurrList()
      .map((currency) => ({
        currency,
        value: calcTotalForCurrency(items, currency),
      }));

    return {
      items,
      total,
    }
  };

  /**
   * Clean cart
   */
  const clean = () => {
    session.cart = [];
  };

  return {
    get,
    addItem,
    deleteItem,
    clean,
  };
};
