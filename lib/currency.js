'use strict';

/**
 * Currency module need to be initialized.
 * If module can't get rates first time it throws error.
 * If module uses without initialization it throws error.
 *
 * After getting first rate, starts interval to update rates
 * every ${currencies.updateInterval} ms.
 * If module can't update rates it just don't update rates but don't throw error.
 */

const config = require('config');
const request = require('request-promise');

const Helpers = require('../lib/helpers');

const currencies = config.get('currencies');
const BASE_CURR = 'RUB';
let rates = null;

/**
 * Get rates
 * @returns {Promise<{}>}
 */
const getRates = async () => {
  const json = await request(currencies.url);
  const valutes = JSON.parse(json)['Valute'];

  let res = {};
  currencies.list.forEach((curr) => {
    if (curr === BASE_CURR) {
      res[curr] = 1;
    } else if (valutes[curr] && valutes[curr]['Value']) {
      res[curr] = valutes[curr]['Value'];
    } else {
      throw new Error(`Can't get rate for currency (${curr})`);
    }
  });

  return res;
};

/**
 * Init module
 * @returns {Promise<void>}
 */
const init = async () => {
  // initial rate
  rates = await getRates();

  // update rates every currencies.updateInterval
  setInterval(async () => {
    try {
      rates = await getRates();
    } catch (err) {
      // just don't save rates
    }
  }, currencies.updateInterval);
};

/**
 * Currency converter
 * @param {number} value
 * @param {string} fromCurr
 * @param {string} toCurr
 * @returns {number}
 */
const convert = (value, fromCurr, toCurr) => {
  if (rates === null) {
    throw new Error('Currencies module has to be initialized before using');
  }

  let result = value;
  if (fromCurr !== toCurr) {
    result = value * rates[fromCurr] / rates[toCurr];
  }

  return Helpers.formatPrice(result);
};

/**
 * Get currencies list
 * @returns {string[]}
 */
const getCurrList = () => {
  return currencies.list;
};

/**
 * Is currency in list
 * @param {string} currency
 * @returns {boolean}
 */
const checkCurrency = (currency) => {
  return currencies.list.includes(currency);
};

module.exports = {
  init,
  convert,
  getCurrList,
  checkCurrency,
  BASE_CURR,
};
