'use strict';

const entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;',
};

module.exports = {
  formatPrice: (value) => Math.ceil(value * 100) / 100,

  escapeHTML: (str) => {
    return ('' + str).replace(/[&<>"'`=\/]/g, (char) => entityMap[char]);
  },
};
