'use strict';

/**
 * Error with status
 * @param {number} status HTTP status
 * @param {string|null} field
 * @param {string} message
 * @constructor
 */
function UserError(status, field = null, message = 'Unknown error') {
  this.name    = 'StatusError';
  this.message = message;
  this.status  = status;
  this.field   = field;
  this.stack   = (new Error()).stack;
}

UserError.prototype = Object.create(Error.prototype);
UserError.prototype.constructor = UserError;

module.exports = UserError;
